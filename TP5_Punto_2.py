import os
from operator import itemgetter, attrgetter

def ingreseOpcion(a,b):
   print('Ingrese opción entre ',a, ' y ', b) 
   nro = int(input())
   while nro < a or nro > b:
         print('Ingrese opción correctamente \n *****************************')
         nro = int(input())
   return nro  

def menu():
   os.system('cls')
   print('*****************TRABAJO DE ORDENACIÓN********************') 
   print('**********ELEGIR UNA OPCIÓN DE LAS DADAS A CONTINUACIÓN**********')
   print('1.Agregar vehiculos')
   print('2.Mostrar lista de vehiculos agregados')
   print('3.Reservar automovil')
   print('4.Buscar automovil por su dominio')
   print('5.Ordenar la lista de automoviles en forma ascendente o descendente por marca') 
   print('6.Ordenar la lista de automoviles en forma ascendente o descendente por precio de venta, mostrando sólo los que se encuentren disponibles')
   print('7.Fin de algoritmo')
   

def pulsarTecla():
    input('Pulse cualquier tecla para continuar...')

def ingreseDominio():
   dominio=input()
   dominio=dominio.replace(" ", "")
   while not(len(dominio)>=6 and len(dominio)<=9):
            dominio=input('Ingrese nuevamente el dominio ')
            dominio=dominio.replace(" ","")
   print(dominio)
   return dominio
  
def validarDominio(vehiculos,dominio):
    validador=True
    for item in vehiculos:
        if dominio==item[0]:
            validador=False
            break
    return validador 

def ingreseMarca():
   marca=input()
   while not (marca in ['R','r', 'F','f', 'C', 'c']):
      marca=input ('Ingrese la marca: R=Renault, F=Ford, C=Citroen: ')
   if marca=='R' or marca=='r':
       marca='Renault'
   elif marca=='F' or marca=='f':
       marca='Ford'
   else:
       marca='Citroen' or marca=='c'
   return marca 


def ingreseTipo():
   tipo=input()
   while not (tipo in ['A','a','U','u' ]):
      tipo=input ('Ingrese tipo de vehiculo: U=Utilitario, A=Automóvil: ')
   if tipo=='U' or 'u':
        tipo='Utilitarios'
   elif tipo=='A' or 'a':
        tipo='Automovil'  
   return tipo
     
def ingreseModelo():
    modelo=int(input())
    while not(modelo>=2005 and modelo<=2020):
        modelo=int(input('Ingrese el modelo [2005-2020]: '))
    return modelo

def ingreseKm():
    kilometraje=int(input()) 
    while not(kilometraje >= 0):
        kilometraje=int(input('Ingrese el kilometraje: '))
    return kilometraje

def ingresePrecioVal():
   precioval=int(input())   
   while not(precioval >= 10000):
        precioval=int(input('Ingrese el precio valuado: '))
   return precioval

def precioVenta(precioval):
  preciovent=int(precioval*1.1)
  print(preciovent)

def mostrarvehiculos(lista):
    print('Listado de Automoviles')
    for item in lista:
        print(item)


def registroAutomoviles(vehiculos):
   print('***********Registro de automóviles***********')
   vehiculos  = [['CG276HD', 'Ford', 'Automovil', 2020, 2345, 350000, 385000, 'D'],
                 ['DFTDA5EV', 'Citroen', 'Utilitario', 2016, 10354, 200000, 220000,'D'],
                 ['AR908ED', 'Citroen', 'Automovil', 2013, 15042, 450000, 495000, 'D'],
                 ['WQ595AZ', 'Renault', 'Automovil', 2002, 89012, 150000, 175000,'D'],
                 ['OP304RWF', 'Ford', 'Utilitario', 2011, 43502, 200000, 220000, 'D'],
                 ['CD202GU', 'Renault', 'Utilitario', 2007, 59123, 150000, 175000,'D'],
                 ['AL209DI', 'Citroen', 'Automovil', 2020, 1042, 450000, 495000, 'D'],
                 ['RT512BA', 'Citroen', 'Utilitario', 2001, 98312, 230000, 253000,'D'],
                 ['TR378PL', 'Renault', 'Automovil', 2008, 71394, 310000, 341000,'D'],
                 ['QC978XX', 'Ford', 'Automovil', 2006, 67420, 210000, 231000,'D']
                ]
   respuesta='s'   
   while (respuesta=='s') or (respuesta=='S'):
      print('Ingrese dominio del auto por favor: ') 
      dominio=ingreseDominio()
      if  validarDominio(vehiculos,dominio):
         print('Ingrese marca (R:Renault) (F:Ford) (C:Citroen)')
         marca=ingreseMarca()
         print('Ingrese tipo de vehiculo (U:Utilitario) (A:Automóvil) ')
         tipo=ingreseTipo()
         print('Ingrese el modelo del auto [2005-2020]: ')
         modelo=ingreseModelo()
         print('Ingrese kilometraje por favor: ')
         kilometraje=ingreseKm()
         print('Ingrese el precio valuado: ')
         precioval=ingresePrecioVal()
         preciovent=int(precioval *1.1)  
         estado='D'
         vehiculos.append([dominio,marca,tipo,modelo,kilometraje,precioval,preciovent, estado])
         print('Auto agregado con éxito')
      else:
         print('Dominio Existente! ')
      respuesta=input('Desea continuar cargando vehiculos? s/n: ')       
   return vehiculos

def buscar(vehiculos,dominio):
    pos=-1
    for i, item in enumerate(vehiculos):
        if(item[0]==dominio):
            pos=i
            break
    return pos  

def buscarPorDominio(vehiculos):
    print('***************Busqueda de Automovil*********************')
    dominio=input('Ingrese dominio para buscar: ')
    pos=buscar(vehiculos,dominio)
    if (pos!=-1):
        print(vehiculos[pos])
    else:
        print('Vehiculo Inexistente')

def reservarAutomovil(vehiculos):
   print('Ingrese automovil para reservar: ')
   dominio=input('Ingrese Dominio: ')
   pos=buscar(vehiculos,dominio)
   if (pos!=-1) and ((vehiculos[pos][7]!='R') or (vehiculos[pos][7]=='V')):
        vehiculos[pos][7]='R'
        print('Vehiculo Reservado con exito')
   else:
        print('Automovil Inexistente o ya se encuentra Reservado o Vendido')
 

def ordenarPorMarca(vehiculos):
   print('Ordenar por marca de vehiculo')
   resp=int(input(' Ingresar forma en que desea ordenar la lista (1:Forma Ascendente) (2:Forma Descendente)'))
   if resp==1:
      listaOrdenada=sorted(vehiculos,key=itemgetter(1))
      mostrarvehiculos(listaOrdenada)
   elif resp==2:  
      listaOrdenada=sorted(vehiculos,key=itemgetter(1), reverse=True)
      mostrarvehiculos(listaOrdenada)
        
def  mostrarDisponibles(lista):
    print('Listado de Automoviles Disponibles')
    for item in lista:
        if item[7]=='D':
            print(item) 

def ordenarPorPrecioVenta(vehiculos):
   print('Ordenar por precio de venta de cada vehiculo')
   resp= int(input('Ingrese la forma en que desea ordenar la lista (1:Forma Ascendente) (2:Forma Descendente)'))
   if resp==1:
      listaOrdenada = sorted(vehiculos,key=itemgetter(6)) 
      mostrarDisponibles(listaOrdenada)
   elif resp==2:  
      listaOrdenada = sorted(vehiculos,key=itemgetter(6), reverse=True) 
      mostrarDisponibles(listaOrdenada)  
  
#Principal
opcion=0
vehiculos=[]
while opcion!=7:
    menu()
    opcion=ingreseOpcion(1,7)
    if opcion==1: 
        vehiculos=registroAutomoviles(vehiculos)
        pulsarTecla()
    elif opcion==2: 
        mostrarvehiculos(vehiculos)
        pulsarTecla()
    elif opcion==3: 
          reservarAutomovil(vehiculos)
          pulsarTecla()
    elif opcion==4: 
        buscarPorDominio(vehiculos)
        pulsarTecla()
    elif opcion==5:
        ordenarPorMarca(vehiculos)
        pulsarTecla()
    elif opcion==6: 
       ordenarPorPrecioVenta(vehiculos)
       pulsarTecla()
    elif opcion==7:
        print('EJECUCION FINALIZADA')
