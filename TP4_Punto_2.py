import os
def ingreseCodigo(dictProductos):
   numCodigo = input()
   n=1
   while n==1:
      if numCodigo in dictProductos:
         numCodigo = input('Ingrese código correctamente(éste ya se halla en lista):   ') 
      elif numCodigo == '':
         numCodigo = input('Ingrese código correctamente:   ')   
      else:
         n=0  
   return numCodigo   

def ingreseDescripcion():
   description = input()
   while description == '':
      description = input('Ingrese alguna descripción por favor:   ')
   return description     

def ingreseNumero():                         
   nro = float(input())
   while nro <= 0:
      nro = float(input('Ingrese valor no negativo por favor:   '))
   return nro

def ingreseEntero():                         
   nro = int(input())
   while nro < 0:
      nro = int(input('Ingrese valor no negativo por favor:   '))
   return nro
            
def registroProductos(dictProductos):
   print('Ingrese código del producto a registrar  por favor') 
   codigo = ingreseCodigo(dictProductos)
   print('Ingrese descripción del producto por favor')
   descripcion = ingreseDescripcion()
   print('Ingrese precio del producto por favor')
   precio = ingreseNumero()
   print('Ingrese stock del producto(cantidad de unidades que ingresan del producto)')
   stock = ingreseEntero()
   dictProductos[codigo]=[descripcion,precio,stock]

def mostrarTodosLosProductos(dictProductos):
   print('**********************LISTA DE PRODUCTOS**********************') 
   for clave, valor in dictProductos.items():
      print('Código de producto: ', clave,'   Info.:', valor) 

def mostrarProductos(limite, dictProductos):
   for clave, valor in dictProductos.items():
      if valor[2] < limite:
         print('Código de producto: ', clave,'   Info.:', valor)

def ingresoIntervalo():
   A=-1
   while A<0:
      A = int(input('Ingrese el inicio del intervalo \n '))
      if A<0:
         print('Ingrese correctamente por favor \n')
   B=-1
   while B<A:
      B = int(input('Ingrese la cota superior del intervalo:\n '))
      if B<A:
         print('Ingrese correctamente por favor \n')      
   return A,B

def productosConStockEnCiertoIntervalo(dictProductos):
   print('Ingrese límites de stock en los productos a mostrar') 
   lim1,lim2 = ingresoIntervalo() 
   print('**********************Productos con un stock entre ', lim1,' y ', lim2, '**********************') 
   for clave, valor in dictProductos.items():
      if valor[2] >= lim1 and valor[2] <= lim2:   
         print('Código de producto: ', clave,'   Info.:', valor)  

def aumentarStock(dictProductos):
   print('Ingrese el límite de valor de los stock a aumentar') 
   Y = ingreseEntero()
   print('Ingrese cantidad de stock que desea aumentar a los siguientes productos: ') 
   mostrarProductos(Y, dictProductos)
   X = ingreseEntero()
   for clave, valor in dictProductos.items():
      if valor[2] < Y:
         dictProductos[clave][2]+=X
   print('Se acaba de sumar ', X, ' unidades a los stock de los productos anteriores')

def eliminarProductos(dictProductos):
   for clave, valor in dictProductos.items():
      if valor[2] == 0 :
         del dictProductos[clave]
         break

def ingreseOpcion(a,b): #valida una opcion del menu para operar la lista
   print('Ingrese opción entre ',a, ' y ', b) 
   nro = int(input())
   while nro < a or nro > b:
         print('Ingrese opción correctamente \n *****************************')
         nro = int(input())
   return nro  

def menu():
   print('******************LISTA DE PRODUCTOS EN UN COMERCIO********************') 
   print('Ingrese una opción para operar:')
   print('  1.Registrar productos \n  2.Mostrar el listado de productos' )
   print('  3.Mostrar productos con un cierto stock')
   print('  4.Sumar stock a productos que no alcancen cierta cantidad \n  5.Eliminar todos los productos cuyo stock sea igual a cero')
   print('  6.Finalizar')

os.system('cls')
productos = {}
n=0
while n==0:
   print('***************OPERACIONES DE DICCIONARIOS(Punto 2)****************')
   print('*******************************************************************')
   menu()
   option = ingreseOpcion(1,6)
   if option==1:
      registroProductos(productos)
   elif option==2:
      mostrarTodosLosProductos(productos)
   elif option==3:
      productosConStockEnCiertoIntervalo(productos)
   elif option==4:
      aumentarStock(productos)  
   elif option==5:
      eliminarProductos(productos)     
   else:
      n=1

          



               